# SASDisplayKit

Currently SmartAd doesn't provide SASDisplayKit SDK as Swift package manager. 
This repository is an alternate version to use the SDK with Swift package manager. 
The Framework is updated directly from the original library, ensuring security.

## Curent Version
- SASDisplayKit: 7.21.0
